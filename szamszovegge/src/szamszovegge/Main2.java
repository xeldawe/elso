package szamszovegge;

public class Main2 {
	static int szam = 730;
	public static void main(String[] args) {
		String szamStr = "" + szam;
		int[] szamok = new int[szamStr.length()];

		String[] szam1 = { "egy", "ketto", "harom", "negy", "ot", "hat", "het", "nyolc", "kilenc" };
		String[] szam2 = { "tiz", "husz", "harminc", "negyven", "otven", "hatvan", "hetven", "nyolcvan", "kilencven" };
		String[] szam3 = { "tizen", "huszon", "harminc", "negyven", "otven", "hatvan", "hetven", "nyolcvan", "kilencven" };
		String[] szam4 = { "szaz", "ketszaz", "haromszaz", "negyszaz", "otszaz", "hatszaz", "hetszaz", "nyolcszaz", "kilencszaz" };

		System.out.println("Fut");
		System.out.println("==================================");

		// Feltolt

		for (int i = 0; i < szamStr.length(); i++) {
			szamok[i] = Character.getNumericValue(szamStr.charAt(i));
			System.out.println("Szamok i: " + szamok[i]);
		}

		if (szamStr.length() == 1)
			System.out.println(szam1[szamok[0] - 1]);

		if (szamStr.length() == 2) {
			if (szamok[1] == 0)
				System.out.println(szam2[szamok[0] - 1]);
			else
				System.out.println(szam3[szamok[0] - 1] + "" + szam1[szamok[1] - 1]);
		}

		if (szamStr.length() == 3) {
			if (szamok[1] == 0) {
				if (szamok[2] == 0)
					System.out.println(szam4[szamok[0] - 1]);
				else
					System.out.println(szam4[szamok[0] - 1] + "" + szam1[szamok[2] - 1]);
			}
			if (szamok[2] == 0) {
				System.out.println(szam4[szamok[0] - 1] + "" + szam2[szamok[1] - 1]);
			} else
				System.out.println(szam4[szamok[0] - 1] + "" + szam3[szamok[1] - 1] + "" + szam1[szamok[2] - 1]);
		}
	}

}
